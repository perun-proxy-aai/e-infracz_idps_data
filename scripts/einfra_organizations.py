import os
import re
import xml.etree.ElementTree as ET

import requests
import yaml


class IndentDumper(yaml.Dumper):
    def increase_indent(self, flow=False, indentless=False):
        return super(IndentDumper, self).increase_indent(flow, False)


metadata_urls = [
    "https://metadata.eduid.cz/entities/eduid+idp",
    "https://metadata.eduid.cz/entities/edugain+idp",
]

ns_metadata = "{urn:oasis:names:tc:SAML:2.0:metadata}"
ns_metadata_rpi = "{urn:oasis:names:tc:SAML:metadata:rpi}"

output_dir = "einfra_organizations"
output_file = output_dir + "/result.yaml"
exception_datafile = "einfra_organizations/exceptions_data.yaml"

default_search_attr = "OrganizationDisplayName"

search_attr_map = {"http://www.eduid.cz/": "OrganizationName"}

res = {}
exception_data = {}

for metadata_url in metadata_urls:
    resp = requests.get(metadata_url)
    metadata = ET.fromstring(resp.content)
    ok = 0
    nok = 0

    with open(exception_datafile) as f:
        documents = yaml.full_load(f)

        if documents is not None:
            for item, doc in documents.items():
                exception_data[item] = doc

    for child in metadata.findall(ns_metadata + "EntityDescriptor"):
        entity_id = child.get("entityID")

        if metadata_url == "https://metadata.eduid.cz/entities/eduid+idp":
            registration_authority = "http://www.eduid.cz/"
        else:
            registration_authority = (
                child.find(ns_metadata + "Extensions")
                .find(ns_metadata_rpi + "RegistrationInfo")
                .get("registrationAuthority")
            )

        idpsso_descriptor_obj = child.find(ns_metadata + "IDPSSODescriptor")
        if idpsso_descriptor_obj is None:
            print(f"Skip metadata with entityId: {entity_id}")
            print()
            nok = nok + 1
            continue
        idpsso_extensions_obj = idpsso_descriptor_obj.find(ns_metadata + "Extensions")
        if idpsso_extensions_obj is None:
            print(f"Skip metadata with entityId: {entity_id}")
            print()
            nok = nok + 1
            continue

        scope_obj = idpsso_extensions_obj.findall(
            "{urn:mace:shibboleth:metadata:1.0}Scope"
        )
        if scope_obj is None:
            print(f"Skip metadata with entityId: {entity_id}")
            print()
            nok = nok + 1
            continue

        organizations = child.find(ns_metadata + "Organization")

        search_attr = default_search_attr
        if registration_authority in search_attr_map.keys():
            search_attr = search_attr_map[registration_authority]

        org_names_xml = organizations.findall(ns_metadata + search_attr)
        org_names = {}
        for item in org_names_xml:
            lang = item.get("{http://www.w3.org/XML/1998/namespace}lang")
            org_names[lang] = item.text

        for item in scope_obj:
            scope = item.text
            if re.search("[*${}]", scope) is not None:
                print(f"Scope is regex: {scope}")
                print()
                continue

            if scope is None or scope in exception_data.keys():
                print(
                    f"Skipping scope {scope}, because it is None or in " f"exceptions."
                )
                print()
                continue

            if scope in res.keys() and res[scope] != org_names:
                print(f"Org with scope and with another " f"names is in result {scope}")
                print(f"Old data: {res[scope]}")
                print(f"New data: {org_names}")
                print()
                nok = nok + 1
                continue
            res[scope] = org_names
            ok = ok + 1

for scope, org_names in exception_data.items():
    res[scope] = org_names

if not os.path.exists(output_dir):
    os.mkdir(output_dir)

with open(output_file, "w") as file:
    documents = yaml.dump(res, file, Dumper=IndentDumper, allow_unicode=True)

print(f"OK: {ok} / NOK: {nok}")

print(len(res.keys()))
